CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage

INTRODUCTION
------------

This module provides timezone calculator widget. User can able to give any datetime as input and they can see the dates in selected timezones as output.


INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload this module to the modules directory of your Drupal
   installation.

 2. Enable the module in 'Extend'.
   (/admin/modules)


USAGE
-------------

 * After installting this module, a block will get created at Block layout.

 * Just place that block at any region.