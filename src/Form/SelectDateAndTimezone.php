<?php

namespace Drupal\timezone_calculator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class SelectDateAndTimezone extends FormBase {

  /**
   * The rendering service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs SelectDateAndTimezone object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer interface.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'select_date_timezone';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $timezones = system_time_zones();

    $form['#prefix'] = '<div id="modal_form_timezone">';
    $form['#suffix'] = '</div>';

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Please select the input and output dates.'),
    ];

    $form['select_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Select Date'),
      '#description' => $this->t('Select the date for which you want to see the timezones'),
      '#required' => TRUE,
    ];

    $form['select_input_timezone'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Input Timezone'),
      '#description' => $this->t('Select the timezone of the input given'),
      '#required' => TRUE,
      '#options' => $timezones,
    ];

    $form['select_output_timezones'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Output Timezones'),
      '#description' => $this->t('Select the timezones in which the date should be displayed'),
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#options' => $timezones,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => [$this, 'checkTimezones'],
      ],
    ];

    return $form;

  }

  /**
   * FormInterface submitForm method must be declared.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * AJAX callback handler that handles content feedback submission.
   */
  public function checkTimezones(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_form_timezone', $form));
    }
    else {
      $timezone_output = '';
      $date_time_array = explode(" ", $form_state->getValue('select_date'));
      $dtUtcDate = strtotime($date_time_array[0] . ' ' . $date_time_array[1] . ' ' . $form_state->getValue('select_input_timezone'));
      $selected_timezones = $form_state->getValue('select_output_timezones');
      $timezone_output .= '<table><thead><th>DateTime</th><th>Timezone</th></thead><tbody>';
      foreach ($selected_timezones as $selected_timezone) {
        // $timezone = $this->date_formatter->format($dtUtcDate, 'custom', 'Y-m-d H:i:s T', $selected_timezone);
        $timezone = \Drupal::service('date.formatter')->format($dtUtcDate, 'custom', 'Y-m-d H:i:s T', $selected_timezone);
        // $timezone = $this->dateFormatter->format($dtUtcDate, 'custom', 'Y-m-d H:i:s T', $selected_timezone);
        $timezone_output .= '<tr><td>' . $timezone . '</td><td>' . $selected_timezone . '</td></tr>';
      }
      $timezone_output .= '</tbody></table>';
      $element = [
        '#type' => 'markup',
        '#markup' => $timezone_output,
        '#prefix' => '<div id="success-message">',
        '#suffix' => '</div>',
      ];
      $response->addCommand(new ReplaceCommand('#modal_form_timezone', $this->renderer->render($element)));
    }

    return $response;
  }

}
