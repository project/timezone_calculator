<?php

namespace Drupal\timezone_calculator\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'TimezoneCalculator' Block.
 *
 * @Block(
 *   id = "timezone_calculator_block",
 *   admin_label = @Translation("Timezone Calculator"),
 *   category = @Translation("Timezone Calculator"),
 * )
 */
class DisplayTimezoneBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = Url::fromRoute('timezone_calculator.select_date_timezone_form');
    $staticTimezoneLink = $this->t('<a class="use-ajax" 
      data-dialog-options="{&quot;width&quot;:400}" 
      data-dialog-type="modal" 
      href="@link">
      Show Timezones
    </a>', ['@link' => $url->toString()]);
    return [
      '#markup' => $staticTimezoneLink,
    ];
  }

}